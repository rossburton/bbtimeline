#! /usr/bin/env python3

import re
import datetime
import sys
import pathlib

# Event object encapsulating the information
# Pluggable parsers.  Input -> list of event objects
# jinja2 to write output
# allow arbitrary arguments passed from caller to template

def explode_name(name):
    """
    Turn recipe-version-revision strings into a tuple
    """
    return name.rsplit("-", 2)

class Event:
    def __init__(self, recipe, task, started=None, ended=None, useful=None):
        self.recipe = recipe
        self.task = task
        self.started = started
        self.ended = ended
        if useful is None:
            self.useful = not (task == "do_rm_work" or task.endswith("_setscene"))
        else:
            self.useful = useful

    # Define these so that Events can be keyed on recipe:task
    def __hash__(self):
        return hash((self.recipe, self.task))

    def __eq__(self, other):
        return self.recipe == other.recipe and self.task == other.task

    def __lt__(self, other):
        return self.started < other.started

    def merge_timestamps(self, other):
        self.started = self.started or other.started
        self.ended = self.ended or other.ended

# TODO move filename to constructor
class Parser:
    def parse_file(self, filename):
        pass

class BuildstatsParser(Parser):
    def parse_buildstats(self, filename):
        started = ended = 0

        with open(filename) as stats:
            for line in stats:
                key, value = line.strip().split(":", 1)
                if key == "Started":
                    started = datetime.datetime.fromtimestamp(float(value))
                if key == "Ended":
                    ended = datetime.datetime.fromtimestamp(float(value))
                if started and ended:
                    break
        return started, ended

    def parse_file(self, filename):
        root = pathlib.Path(filename)
        assert root.is_dir()
        for recipedir in root.iterdir():
            if recipedir.is_dir():
                recipe = explode_name(recipedir.name)[0]
                for taskfile in recipedir.glob("do_*"):
                    task = taskfile.name
                    started, ended = self.parse_buildstats(taskfile)
                    yield Event(recipe, task, started, ended)

class CookerLogParser(Parser):
    def line_parser(self, line:str):
        # 2020-11-13 14:18:50 - INFO     - NOTE: recipe kmod-27-r0: task do_populate_lic_setscene: Started
        regex = re.compile(r"^([-\d]{10} [:\d]{8}).+NOTE: recipe (.+): task (.+): (\w+)$")
        match = regex.match(line)
        if match:
            timestamp = datetime.datetime.strptime(match.group(1), "%Y-%m-%d %H:%M:%S")
            recipe = explode_name(match.group(2))[0]
            task = match.group(3)
            is_started = match.group(4) == "Started"
            started = timestamp if is_started else None
            ended = timestamp if not is_started else None
            return Event(recipe, task, started, ended)
        else:
            return None

    def parse_file(self, filename):
        with open(filename) as f:
            for line in f:
                event = self.line_parser(line)
                # TODO: coalesce here?
                if event:
                    yield event

template1 = """
<html>
    <head><title>Timeline</title></head>
<body>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
  google.charts.load("current", {packages:["timeline"]});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {

    /* TODO use timeofday instead of datetime? */
    data = {
        "cols": [
            { "id": "Recipe", "type" : "string" },
            { "id": "Task", "type" : "string" },
            { "id": "Start", "type" : "datetime" },
            { "id": "End", "type" : "datetime" },
            { "id": "Useful", "type": "boolean"}
        ],
        "rows" : [
"""
template2 = """
        ]
    };

    usefulCheck = document.querySelector('input[id="useful"]');
    mergeCheck = document.querySelector('input[id="merge"]');
    updateChart = function() {
        var view = new google.visualization.DataView(dataTable);
        if (usefulCheck.checked) {
            view.setRows(view.getFilteredRows([{column: 4, value: true}]));
        }
        view.hideColumns([4]);

        var options = {
            timeline: { groupByRowLabel: mergeCheck.checked,
                        avoidOverlappingGridLines: false},
            hAxis: {
                format: "HH:mm:ss"
            }
        };

        chart.draw(view, options);
    };

    var dataTable = new google.visualization.DataTable(data);
    var container = document.getElementById('chart');
    var chart = new google.visualization.Timeline(container);
    updateChart();
  }
</script>

<input id="useful" checked type="checkbox" onclick="updateChart()"/>
<label for="useful">Hide useless tasks</label>

<input id="merge" checked type="checkbox" onclick="updateChart()"/>
<label for="merge">Merge tasks</label>

<div id="chart" style="height: 600px;"></div>
</body>
</html>
"""

def write_html(events):
    def date_to_java(dt):
        return (f"new Date({int(dt.timestamp() * 1000)})")

    # events is a list of events
    print(template1)
    for e in events:
        print(f"""{{'c': [
            {{'v': '{e.recipe}' }},
            {{'v': '{e.task}' }},
            {{'v': {date_to_java(e.started)} }},
            {{'v': {date_to_java(e.ended)} }},
            {{'v': {e.useful and 'true' or 'false'} }},
        ]}},""")
    print(template2)

def reset_origin(events):
    """
    Iterate through all events, resetting the start to the epoch.
    """
    events = sorted(events, key=lambda e: e.started)
    origin = events[0].started - datetime.datetime.fromtimestamp(0)
    for event in events:
        event.started -= origin
        event.ended -= origin
    return events

def validate_timestamps(events):
    for e in events:
        if not e.started or not e.ended:
            return False
    return True

if __name__ == "__main__":
    events = {}
    #parser = CookerLogParser()
    parser = BuildstatsParser()
    for event in parser.parse_file(sys.argv[1]):
        # Merge timestamps
        # TODO do in parser
        try:
            existing = events[event]
            existing.merge_timestamps(event)
        except KeyError:
            events[event] = event

    events = events.values()
    assert validate_timestamps(events)
    events = reset_origin(events)
    write_html(events)
