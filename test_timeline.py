#! /usr/bin/env python3

from bbtimeline import Event, CookerLogParser, reset_origin, explode_name, validate_timestamps, BuildstatsParser

import datetime
import unittest

# Convenience function to make datetimes from timestamps
dt = datetime.datetime.fromtimestamp

class ValidateTest(unittest.TestCase):
    def test_validate(self):
        events = [Event("recipe", "task", None, None)]
        self.assertFalse(validate_timestamps(events))

        events = [Event("recipe", "task", None, 1)]
        self.assertFalse(validate_timestamps(events))

        events = [Event("recipe", "task", 1, None)]
        self.assertFalse(validate_timestamps(events))

        events = [Event("recipe", "task", 1, 2)]
        self.assertTrue(validate_timestamps(events))

class ExplodeTest(unittest.TestCase):
    def test_explode(self):
        recipe, version, revision = explode_name("perf-1.0-r9")
        self.assertEqual(recipe, "perf")
        self.assertEqual(version, "1.0")
        self.assertEqual(revision, "r9")

class EventTests(unittest.TestCase):
    def test_merge(self):
        e1 = Event("recipe", "task", 1, None)
        e2 = Event("recipe", "task", None, 2)
        self.assertEqual(e1.started, 1)
        self.assertIsNone(e1.ended)
        e1.merge_timestamps(e2)
        self.assertEqual(e1.started, 1)
        self.assertEqual(e1.ended, 2)

    def test_useful(self):
        # Check defaults
        self.assertTrue(Event("recipe", "do_unpack").useful)
        self.assertFalse(Event("recipe", "do_unpack_setscene").useful)
        self.assertFalse(Event("recipe", "do_rm_work").useful)

        # Check explicit assignments
        self.assertFalse(Event("recipe", "do_unpack", useful=False).useful)
        self.assertTrue(Event("recipe", "do_unpack_setscene", useful=True).useful)

    def test_hash(self):
        e1 = Event("recipe", "task", 1, None)
        e2 = Event("recipe", "task", None, 2)
        e3 = Event("anotherrecipe", "task", None, 2)

        self.assertEqual(hash(e1), hash(e2))
        self.assertNotEqual(hash(e1), hash(e3))

        d = {}
        d[e1] = e1
        self.assertIn(e1, d)
        self.assertIn(e2, d)
        self.assertNotIn(e3, d)

    def test_reset_origin(self):
        e1 = Event("second-long", "task", dt(1000), dt(2000))
        e2 = Event("first-short", "task", dt(500), dt(505))
        events = reset_origin([e1, e2])
        self.assertEqual(events[0].recipe, "first-short")
        self.assertEqual(events[0].started, dt(0))
        self.assertEqual(events[0].ended, dt(5))
        self.assertEqual(events[1].recipe, "second-long")
        self.assertEqual(events[1].started, dt(500))
        self.assertEqual(events[1].ended, dt(1500))

class CookerParserTests(unittest.TestCase):
    def test_line_parser(self):
        parser = CookerLogParser()

        self.assertIsNone(parser.line_parser("This is not a valid line"))

        event = parser.line_parser("2020-11-13 14:18:50 - INFO     - NOTE: recipe kmod-27-r0: task do_populate_lic_setscene: Started")
        self.assertEqual(event.recipe, "kmod")
        self.assertEqual(event.task, "do_populate_lic_setscene")
        self.assertEqual(event.started, datetime.datetime(2020, 11, 13, 14, 18, 50))
        self.assertIsNone(event.ended)

    def test_parser(self):
        parser = CookerLogParser()
        it = parser.parse_file("test/cooker.log")

        event = next(it)
        self.assertEqual(event.recipe, "popt")
        self.assertEqual(event.task, "do_populate_lic")
        self.assertEqual(event.started, datetime.datetime(2020, 11, 16, 16, 52, 45))
        self.assertIsNone(event.ended)

        event = next(it)
        self.assertEqual(event.recipe, "popt")
        self.assertEqual(event.task, "do_populate_lic")
        self.assertIsNone(event.started)
        self.assertEqual(event.ended, datetime.datetime(2020, 11, 16, 16, 52, 55))
        with self.assertRaises(StopIteration):
            next(it)

class BuildstatsParserTests(unittest.TestCase):
    def test_parser(self):
        parser = BuildstatsParser()
        events = sorted(parser.parse_file("test/buildstats"))
        self.assertEqual(len(events), 4)

        def compare(event, recipe, task, started, ended):
            self.assertEqual(event.recipe, recipe)
            self.assertEqual(event.task, task)
            self.assertEqual(event.started, started)
            self.assertEqual(event.ended, ended)

        compare(events[0], "readline-native", "do_populate_lic_setscene", dt(1605616920.27), dt(1605616920.44))
        compare(events[1], "readline-native", "do_populate_sysroot", dt(1605616922.08), dt(1605616922.36))
        compare(events[2], "rsync-native", "do_populate_sysroot", dt(1605616922.34), dt(1605616922.68))
        compare(events[3], "readline-native", "do_rm_work", dt(1605616930.97), dt(1605616931.09))
